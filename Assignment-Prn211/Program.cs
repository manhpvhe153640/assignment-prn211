internal class Program
{
    private static void Main(string[] args)
    {

        var builder = WebApplication.CreateBuilder(args);
        builder.Services.AddControllersWithViews();
        builder.Services.AddSession();
        builder.Services.AddSingleton<IHttpContextAccessor,HttpContextAccessor>();
        var app = builder.Build();


        app.UseSession();

        app.MapControllerRoute(
           name: "2part",
           pattern: "/{controller=Home}/{action=Home}"
           );

        app.Run();
    }
}