﻿using Assignment_Prn211.models;

namespace Assignment_Prn211.Logics
{
    public class CategoryManage
    {
        public List<Category> GetCategories()
        {
            using (var context = new SNSSneakerContext())
            {
                return context.Categories.ToList();
            }
        }
    }
}
