﻿using Assignment_Prn211.models;
using Microsoft.AspNetCore.Identity;

namespace Assignment_Prn211.Logics
{
    public class UserManage
    {
        public List<Category> GetCategories()
        {
            using (var context = new SNSSneakerContext())
            {
                return context.Categories.ToList();
            }
        }

        public User GetUser(string username,string password)
        {
            using (var context = new SNSSneakerContext())
            {
                return context.Users.FirstOrDefault(x => x.Password == password && x.Username == username);
            }
        }

        public User GetUserByUsername(string username)
        {
            using (var context = new SNSSneakerContext())
            {
                return context.Users.FirstOrDefault(x => x.Username == username);
            }
        }

        public void UpdateUser(User u1)
        {
            using (var context = new SNSSneakerContext())
            {
                User u = context.Users.FirstOrDefault(x => x.Password == u1.Password && x.Username == u1.Username);
                u.Fullname = u1.Fullname;
                u.Phone = u1.Phone;
                u.Address = u1.Address;
                u.Email = u1.Email;


                context.Update(u);
                context.SaveChanges();
            }
        }

        public void AddUser(User u)
        {
            using (var context = new SNSSneakerContext())
            {
                context.Add(u);
                context.SaveChanges();
            }
        }

    }
}
