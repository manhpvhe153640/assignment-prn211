﻿using Assignment_Prn211.models;

namespace Assignment_Prn211.Logics
{
    public class ProductManage
    {
        public List<Product> GetProducts(string Id, int Offset, int Size,string search)
        {
            using (var context = new SNSSneakerContext())
            {
                context.Categories.ToList();

                if (Id == "" || Id == null)
                    return context.Products.Skip(Offset - 1).Take(Size).ToList();
                else return context.Products.Where(x => x.Cid == Id && x.Name.Contains(search)).Skip(Offset - 1).Take(Size).ToList();
            }
        }

        public int GetNumberOfProduct(string cID,string search)
        {
            var context = new SNSSneakerContext();
            if (cID == "" || cID == null)
                return context.Products.Count(x => x.Name.Contains(search));
            else
                return context.Products.Where(x => x.Cid == cID && x.Name.Contains(search)).Count();
        }
    }
}
