﻿using Assignment_Prn211.Logics;
using Assignment_Prn211.models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Assignment_Prn211.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration configuration;
        public HomeController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public IActionResult Home()
        {

            int page = 1;


            string search = HttpContext.Request.Query["search"];
            if(search == null)
            {
                search = "";
            }
            ViewBag.Search = search;    
            string page_raw = HttpContext.Request.Query["page"];
            if (page_raw != null)
            {
                page = Convert.ToInt32(page_raw);
                ViewBag.Page = page;
            }
            string id_raw = HttpContext.Request.Query["Id"];
            
            ProductManage productManage = new ProductManage();
            
            int PageSize = Convert.ToInt32(configuration.GetValue<string>("AppSettings:PageSize"));

            if (page <= 0) page = 1;
            List<Product> products = productManage.GetProducts(id_raw, (page - 1) * PageSize + 1, PageSize,search);
            CategoryManage categoryManage = new CategoryManage();
            List<Category> categories = categoryManage.GetCategories();
            ViewBag.Categories = categories;


            int TotalOrder = productManage.GetNumberOfProduct(id_raw,search);
            int TotalPage = (TotalOrder / PageSize);
            if (TotalOrder % PageSize != 0) TotalPage++;
            ViewData["TotalPage"] = TotalPage;
            ViewData["CurrentPage"] = page;
            ViewData["CurrentCid"] = id_raw;

            return View(products);
        }
    }
}
