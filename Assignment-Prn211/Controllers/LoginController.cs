﻿using Assignment_Prn211.Logics;
using Assignment_Prn211.models;
using Microsoft.AspNetCore.Mvc;

namespace Assignment_Prn211.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Login()
        {
            string error = HttpContext.Request.Query["error"];
            if(error == "true")
            {
                ViewBag.error = "Wrong user name or password";
            }
            return View();
        }

        public IActionResult CheckLogin()
        {
            string userName = HttpContext.Request.Query["username"];
            string password = HttpContext.Request.Query["passworld"];

            UserManage userManage = new UserManage();
            User u = userManage.GetUser(userName,password);
            if(u == null) {
                
                return RedirectToAction("Login",new {error = "true"});
            }
            else
            {
                
                HttpContext.Session.SetString("User",userName);
                HttpContext.Session.SetString("NameUser", u.Fullname);
                HttpContext.Session.SetString("Password", u.Password);
                HttpContext.Session.SetString("Phone", u.Username);
                HttpContext.Session.SetString("Email", u.Email);
                HttpContext.Session.SetString("Address", u.Address);

                return RedirectToAction("Home","Home");
            }
            
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Home", "Home");
        }

        public IActionResult Register()
        {
            if (HttpContext.Request.Query["success"] == "false")
            {
                ViewBag.Success = "User name already exist!";
            }
            return View();
        }

        public IActionResult CheckRegister()
        {
            string userName = HttpContext.Request.Query["user"];

            UserManage userManage = new UserManage();
            if(userManage.GetUserByUsername(userName) != null)
            {
                return RedirectToAction("Register",new {success = "false"});
            }


            string? fullName = HttpContext.Request.Query["name"];
            string? password = HttpContext.Request.Query["password"];
            string? phone = HttpContext.Request.Query["phone"];
            string? email = HttpContext.Request.Query["email"];
            string? address = HttpContext.Request.Query["address"];

            User u = new User();
            u.Address = address;
            u.Roll = "User";
            u.Fullname = fullName;
            u.Username = userName;
            u.Username = userName;
            u.Password = password;
            u.Email = email;

            userManage.AddUser(u);

            HttpContext.Session.SetString("User", userName);
            HttpContext.Session.SetString("NameUser", u.Fullname);
            HttpContext.Session.SetString("Password", u.Password);
            HttpContext.Session.SetString("Phone", u.Username);
            HttpContext.Session.SetString("Email", u.Email);
            HttpContext.Session.SetString("Address", u.Address);


            return RedirectToAction("Home","Home");
        }

    }
}
