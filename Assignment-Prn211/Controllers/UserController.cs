﻿using Assignment_Prn211.Logics;
using Assignment_Prn211.models;
using Microsoft.AspNetCore.Mvc;

namespace Assignment_Prn211.Controllers
{
    public class UserController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Profile()
        {
            if(HttpContext.Request.Query["success"] == "true")
            {
                ViewBag.Success = "Change profile Successful";
            }
            return View();
        }

        public IActionResult EditProfile()
        {
            string userName = HttpContext.Request.Query["user"];
            string? fullName = HttpContext.Request.Query["name"];
            string? password = HttpContext.Session.GetString("Password");
            string? phone = HttpContext.Request.Query["phone"];
            string? email = HttpContext.Request.Query["email"];
            string? address = HttpContext.Request.Query["address"];

            
            UserManage userManage = new UserManage();

            User u1 = new User();
            u1.Username = userName;
            u1.Password = password;
            u1.Fullname = fullName;
            u1.Phone = phone;
            u1.Address = address;
            u1.Email = email;
              

            HttpContext.Session.Clear();

            HttpContext.Session.SetString("User", userName);
            HttpContext.Session.SetString("NameUser", u1.Fullname);
            HttpContext.Session.SetString("Password", u1.Password);
            HttpContext.Session.SetString("Phone", u1.Username);
            HttpContext.Session.SetString("Email", u1.Email);
            HttpContext.Session.SetString("Address", u1.Address);


            userManage.UpdateUser(u1);
            return RedirectToAction("Profile",new {success = "true"});
        }

    }
}
