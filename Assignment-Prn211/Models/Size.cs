﻿using System;
using System.Collections.Generic;

namespace Assignment_Prn211.models
{
    public partial class Size
    {
        public string? Id { get; set; }
        public string? Pid { get; set; }
        public string? Size1 { get; set; }

        public virtual Product? PidNavigation { get; set; }
    }
}
