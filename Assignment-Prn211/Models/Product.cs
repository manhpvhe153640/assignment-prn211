﻿using System;
using System.Collections.Generic;

namespace Assignment_Prn211.models
{
    public partial class Product
    {
        public string Id { get; set; } = null!;
        public string? Name { get; set; }
        public double? Price { get; set; }
        public string? Describe { get; set; }
        public string? Image { get; set; }
        public int? Quantity { get; set; }
        public string? Cid { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string? Sex { get; set; }

        public virtual Category? CidNavigation { get; set; }
        public virtual ProductSale? ProductSale { get; set; }
    }
}
