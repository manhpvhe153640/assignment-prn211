﻿using System;
using System.Collections.Generic;

namespace Assignment_Prn211.models
{
    public partial class Category
    {
        public Category()
        {
            Products = new HashSet<Product>();
        }

        public string Id { get; set; } = null!;
        public string? Name { get; set; }
        public string? Infomation { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
