﻿using System;
using System.Collections.Generic;

namespace Assignment_Prn211.models
{
    public partial class Ordersdetail
    {
        public int Id { get; set; }
        public int? OrdId { get; set; }
        public string? ProductName { get; set; }
        public double? ProductPrice { get; set; }
        public int? Quantity { get; set; }

        public virtual Order? Ord { get; set; }
    }
}
