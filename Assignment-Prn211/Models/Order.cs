﻿using System;
using System.Collections.Generic;

namespace Assignment_Prn211.models
{
    public partial class Order
    {
        public Order()
        {
            Ordersdetails = new HashSet<Ordersdetail>();
        }

        public int Id { get; set; }
        public string? Username { get; set; }
        public string? Name { get; set; }
        public string? Address { get; set; }
        public string? Phone { get; set; }
        public double? Totalprice { get; set; }

        public virtual User? UsernameNavigation { get; set; }
        public virtual ICollection<Ordersdetail> Ordersdetails { get; set; }
    }
}
