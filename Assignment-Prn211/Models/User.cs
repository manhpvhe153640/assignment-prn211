﻿using System;
using System.Collections.Generic;

namespace Assignment_Prn211.models
{
    public partial class User
    {
        public User()
        {
            Orders = new HashSet<Order>();
        }

        public string Username { get; set; } = null!;
        public string? Password { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
        public string? Address { get; set; }
        public string? Fullname { get; set; }
        public string? Roll { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
