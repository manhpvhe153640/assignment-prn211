﻿using System;
using System.Collections.Generic;

namespace Assignment_Prn211.models
{
    public partial class OrderUser
    {
        public int Id { get; set; }
        public string? Pid { get; set; }
        public double? Price { get; set; }
        public int? Quantity { get; set; }
        public double? Total { get; set; }
        public string? Status { get; set; }
    }
}
