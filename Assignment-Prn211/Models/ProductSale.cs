﻿using System;
using System.Collections.Generic;

namespace Assignment_Prn211.models
{
    public partial class ProductSale
    {
        public string Pid { get; set; } = null!;
        public double? PriceSale { get; set; }

        public virtual Product PidNavigation { get; set; } = null!;
    }
}
