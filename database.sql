USE [master]
GO
/****** Object:  Database [SNSSneaker]    Script Date: 10/30/2022 7:49:37 PM ******/
CREATE DATABASE [SNSSneaker]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SNSSnerker', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS01\MSSQL\DATA\SNSSnerker.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SNSSnerker_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS01\MSSQL\DATA\SNSSnerker_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [SNSSneaker] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SNSSneaker].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SNSSneaker] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SNSSneaker] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SNSSneaker] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SNSSneaker] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SNSSneaker] SET ARITHABORT OFF 
GO
ALTER DATABASE [SNSSneaker] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [SNSSneaker] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SNSSneaker] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SNSSneaker] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SNSSneaker] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SNSSneaker] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SNSSneaker] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SNSSneaker] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SNSSneaker] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SNSSneaker] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SNSSneaker] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SNSSneaker] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SNSSneaker] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SNSSneaker] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SNSSneaker] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SNSSneaker] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SNSSneaker] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SNSSneaker] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SNSSneaker] SET  MULTI_USER 
GO
ALTER DATABASE [SNSSneaker] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SNSSneaker] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SNSSneaker] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SNSSneaker] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SNSSneaker] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [SNSSneaker] SET QUERY_STORE = OFF
GO
USE [SNSSneaker]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 10/30/2022 7:49:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[ID] [varchar](20) NOT NULL,
	[Name] [varchar](max) NULL,
	[Infomation] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 10/30/2022 7:49:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](20) NULL,
	[Name] [varchar](20) NULL,
	[Address] [varchar](20) NULL,
	[Phone] [varchar](20) NULL,
	[totalprice] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordersdetails]    Script Date: 10/30/2022 7:49:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordersdetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ordID] [int] NULL,
	[productName] [varchar](max) NULL,
	[productPrice] [float] NULL,
	[quantity] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderUser]    Script Date: 10/30/2022 7:49:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[pid] [varchar](20) NULL,
	[price] [float] NULL,
	[quantity] [int] NULL,
	[total] [float] NULL,
	[status] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 10/30/2022 7:49:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ID] [varchar](20) NOT NULL,
	[Name] [varchar](max) NULL,
	[Price] [float] NULL,
	[Describe] [varchar](max) NULL,
	[image] [varchar](max) NULL,
	[quantity] [int] NULL,
	[Cid] [varchar](20) NULL,
	[releaseDate] [date] NULL,
	[sex] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductSale]    Script Date: 10/30/2022 7:49:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductSale](
	[pid] [varchar](20) NOT NULL,
	[priceSale] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[pid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Size]    Script Date: 10/30/2022 7:49:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Size](
	[ID] [varchar](20) NULL,
	[pid] [varchar](20) NULL,
	[Size] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 10/30/2022 7:49:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Username] [varchar](20) NOT NULL,
	[Password] [varchar](20) NULL,
	[Phone] [varchar](20) NULL,
	[Email] [varchar](20) NULL,
	[address] [varchar](max) NULL,
	[Fullname] [varchar](max) NULL,
	[roll] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Category] ([ID], [Name], [Infomation]) VALUES (N'ad', N'Adidas', N'Running')
INSERT [dbo].[Category] ([ID], [Name], [Infomation]) VALUES (N'cv', N'Converse', N'Unisex')
INSERT [dbo].[Category] ([ID], [Name], [Infomation]) VALUES (N'JD', N'Jordan', N'NikeJodan')
INSERT [dbo].[Category] ([ID], [Name], [Infomation]) VALUES (N'nk', N'Nike', N'sb dunk, air force')
INSERT [dbo].[Category] ([ID], [Name], [Infomation]) VALUES (N'pm', N'Puma', N'Unisex')
INSERT [dbo].[Category] ([ID], [Name], [Infomation]) VALUES (N'vs', N'Vans', N'Unisex')
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([ID], [Username], [Name], [Address], [Phone], [totalprice]) VALUES (1, N'sa', N'Nguyen van a', N'098234123', N'ho chi minh', 2840)
INSERT [dbo].[Orders] ([ID], [Username], [Name], [Address], [Phone], [totalprice]) VALUES (2, N'sa', N'', N'', N'', 2100)
INSERT [dbo].[Orders] ([ID], [Username], [Name], [Address], [Phone], [totalprice]) VALUES (3, N'sa', N'', N'', N'', 2100)
INSERT [dbo].[Orders] ([ID], [Username], [Name], [Address], [Phone], [totalprice]) VALUES (5, N'sa', N'aaa', N'0989498906', N'aa', 120)
INSERT [dbo].[Orders] ([ID], [Username], [Name], [Address], [Phone], [totalprice]) VALUES (6, N'sb', N'nguyen van a', N'1234567', N'ho chi minh', 3500)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[Ordersdetails] ON 

INSERT [dbo].[Ordersdetails] ([ID], [ordID], [productName], [productPrice], [quantity]) VALUES (1, 1, N'Adidas F1', 1900, 1)
INSERT [dbo].[Ordersdetails] ([ID], [ordID], [productName], [productPrice], [quantity]) VALUES (2, 1, N'Adidas LH', 1420, 2)
INSERT [dbo].[Ordersdetails] ([ID], [ordID], [productName], [productPrice], [quantity]) VALUES (3, 2, N'Adidas S1', 2100, 1)
INSERT [dbo].[Ordersdetails] ([ID], [ordID], [productName], [productPrice], [quantity]) VALUES (4, 3, N'Adidas S1', 2100, 1)
INSERT [dbo].[Ordersdetails] ([ID], [ordID], [productName], [productPrice], [quantity]) VALUES (5, 5, N'Vans 6', 120, 1)
INSERT [dbo].[Ordersdetails] ([ID], [ordID], [productName], [productPrice], [quantity]) VALUES (6, 6, N'Jodan 2', 3500, 1)
SET IDENTITY_INSERT [dbo].[Ordersdetails] OFF
GO
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'ad1', N'Adidas S1', 2100, N'Everything we do is rooted in sport. Sport plays an increasingly important role in more and more people’s lives, on and off the field of play. It is central to every culture and society and is core to our health and happiness.', N'Screenshot 2022-06-18 153730.png', 0, N'ad', CAST(N'2019-12-01' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'ad10', N'Adidas F1', 1900, N'Everything we do is rooted in sport. Sport plays an increasingly important role in more and more people’s lives, on and off the field of play. It is central to every culture and society and is core to our health and happiness.', N'Screenshot 2022-06-18 153848.png', 3, N'ad', CAST(N'2018-05-01' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'ad2', N'Adidas LH', 1420, N'Everything we do is rooted in sport. Sport plays an increasingly important role in more and more people’s lives, on and off the field of play. It is central to every culture and society and is core to our health and happiness.', N'Screenshot 2022-06-18 153817.png', 5, N'ad', CAST(N'2019-03-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'ad3', N'Adidas SQ', 2155, N'Everything we do is rooted in sport. Sport plays an increasingly important role in more and more people’s lives, on and off the field of play. It is central to every culture and society and is core to our health and happiness.', N'Screenshot 2022-06-18 153833.png', 8, N'ad', CAST(N'2019-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'ad4', N'Adidas Run', 3260, N'Everything we do is rooted in sport. Sport plays an increasingly important role in more and more people’s lives, on and off the field of play. It is central to every culture and society and is core to our health and happiness.', N'Screenshot 2022-06-18 153848.png', 7, N'ad', CAST(N'2019-05-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'ad5', N'Adidas LK', 3150, N'Everything we do is rooted in sport. Sport plays an increasingly important role in more and more people’s lives, on and off the field of play. It is central to every culture and society and is core to our health and happiness.', N'Screenshot 2022-06-18 153848.png', 8, N'ad', CAST(N'2019-08-15' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'ad6', N'Adidas UK', 1600, N'Everything we do is rooted in sport. Sport plays an increasingly important role in more and more people’s lives, on and off the field of play. It is central to every culture and society and is core to our health and happiness.', N'Screenshot 2022-06-18 153848.png', 9, N'ad', CAST(N'2019-07-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'ad7', N'Adidas H2', 1200, N'Everything we do is rooted in sport. Sport plays an increasingly important role in more and more people’s lives, on and off the field of play. It is central to every culture and society and is core to our health and happiness.', N'Screenshot 2022-06-18 153848.png', 2, N'ad', CAST(N'2019-06-15' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'ad8', N'Adidas QE', 1300, N'Everything we do is rooted in sport. Sport plays an increasingly important role in more and more people’s lives, on and off the field of play. It is central to every culture and society and is core to our health and happiness.', N'Screenshot 2022-06-18 153848.png', 1, N'ad', CAST(N'2019-05-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'ad9', N'Adidas SN', 1700, N'Everything we do is rooted in sport. Sport plays an increasingly important role in more and more people’s lives, on and off the field of play. It is central to every culture and society and is core to our health and happiness.', N'Screenshot 2022-06-18 153848.png', 5, N'ad', CAST(N'2018-04-15' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'cv1', N'Converse 1', 200, N'Chuck Taylor All-Stars or Converse All Stars (also referred to as "Converse", "Chuck Taylors", "Chucks", "Cons", "All Stars", and "Chucky Ts") is a model of casual shoe manufactured by Converse (a subsidiary of Nike, Inc. since 2003) that was initially developed as a basketball shoe in the early 20th century.', N'Screenshot 2022-06-18 154803.png', 2, N'cv', CAST(N'2019-01-15' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'cv2', N'Converse 2', 300, N'Chuck Taylor All-Stars or Converse All Stars (also referred to as "Converse", "Chuck Taylors", "Chucks", "Cons", "All Stars", and "Chucky Ts") is a model of casual shoe manufactured by Converse (a subsidiary of Nike, Inc. since 2003) that was initially developed as a basketball shoe in the early 20th century.', N'Screenshot 2022-06-18 154815.png', 2, N'cv', CAST(N'2019-03-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'cv3', N'Converse 3', 400, N'Chuck Taylor All-Stars or Converse All Stars (also referred to as "Converse", "Chuck Taylors", "Chucks", "Cons", "All Stars", and "Chucky Ts") is a model of casual shoe manufactured by Converse (a subsidiary of Nike, Inc. since 2003) that was initially developed as a basketball shoe in the early 20th century.', N'Screenshot 2022-06-18 154832.png', 3, N'cv', CAST(N'2019-07-15' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'cv4', N'Converse 4', 600, N'Chuck Taylor All-Stars or Converse All Stars (also referred to as "Converse", "Chuck Taylors", "Chucks", "Cons", "All Stars", and "Chucky Ts") is a model of casual shoe manufactured by Converse (a subsidiary of Nike, Inc. since 2003) that was initially developed as a basketball shoe in the early 20th century.', N'Screenshot 2022-06-18 154859.png', 4, N'cv', CAST(N'2019-04-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'cv5', N'Converse 5', 120, N'Chuck Taylor All-Stars or Converse All Stars (also referred to as "Converse", "Chuck Taylors", "Chucks", "Cons", "All Stars", and "Chucky Ts") is a model of casual shoe manufactured by Converse (a subsidiary of Nike, Inc. since 2003) that was initially developed as a basketball shoe in the early 20th century.', N'Screenshot 2022-06-18 154913.png', 5, N'cv', CAST(N'2019-09-15' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'cv6', N'Converse 6', 320, N'Chuck Taylor All-Stars or Converse All Stars (also referred to as "Converse", "Chuck Taylors", "Chucks", "Cons", "All Stars", and "Chucky Ts") is a model of casual shoe manufactured by Converse (a subsidiary of Nike, Inc. since 2003) that was initially developed as a basketball shoe in the early 20th century.', N'Screenshot 2022-06-18 154925.png', 6, N'cv', CAST(N'2019-05-15' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'jd1', N'Jodan 1', 5000, N'The Air Jordan 1 High debuted in 1985 as the first signature sneaker developed by Nike for Michael Jordan.', N'BleachedCoral.jpg', 5, N'JD', CAST(N'2019-06-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'jd10', N'Jodan 10', 1200, N'The Air Jordan 1 High debuted in 1985 as the first signature sneaker developed by Nike for Michael Jordan.', N'WhitePatentLeather.jpg', 1, N'JD', CAST(N'2019-07-15' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'jd2', N'Jodan 2', 3500, N'The Air Jordan 1 High debuted in 1985 as the first signature sneaker developed by Nike for Michael Jordan.', N'Bulls.jpg', 5, N'JD', CAST(N'2022-02-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'jd3', N'Jodan 3', 1000, N'The Air Jordan 1 High debuted in 1985 as the first signature sneaker developed by Nike for Michael Jordan.', N'CardinalRed.jpg', 7, N'JD', CAST(N'2021-02-15' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'jd4', N'Jodan 4', 1200, N'The Air Jordan 1 High debuted in 1985 as the first signature sneaker developed by Nike for Michael Jordan.', N'CoconutMilk.jpg', 9, N'JD', CAST(N'2021-02-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'jd5', N'Jodan 5', 1300, N'The Air Jordan 1 High debuted in 1985 as the first signature sneaker developed by Nike for Michael Jordan.', N'Heritage.jpg', 3, N'JD', CAST(N'2022-02-15' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'jd6', N'Jodan 6', 1500, N'The Air Jordan 1 High debuted in 1985 as the first signature sneaker developed by Nike for Michael Jordan.', N'Linen.jpg', 4, N'JD', CAST(N'2021-02-15' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'jd7', N'Jodan 7', 1500, N'The Air Jordan 1 High debuted in 1985 as the first signature sneaker developed by Nike for Michael Jordan.', N'MarinaBlue.jpg', 2, N'JD', CAST(N'2022-02-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'jd8', N'Jodan 8', 1400, N'The Air Jordan 1 High debuted in 1985 as the first signature sneaker developed by Nike for Michael Jordan.', N'MilitaryBlack.jpg', 1, N'JD', CAST(N'2021-02-15' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'jd9', N'Jodan 9', 1900, N'The Air Jordan 1 High debuted in 1985 as the first signature sneaker developed by Nike for Michael Jordan.', N'WhiteLightSmokeGrey.jpg', 6, N'JD', CAST(N'2021-02-15' AS Date), N'Unisex')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'nk1', N'Nike AIR', 1000, N'Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight. They opened their first retail outlet in 1966 and launched the Nike brand shoe in 1972. The company was renamed Nike, Inc., in 1978 and went public two years later. By the early 21st century, Nike had retail outlets and distributors in more than 170 countries, and its logo—a curved check mark called the “swoosh”—was recognized throughout the world.', N'ArchaeoBrownandRust Pink.jpg', 2, N'nk', CAST(N'2021-02-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'nk10', N'Nike how', 1900, N'Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight. They opened their first retail outlet in 1966 and launched the Nike brand shoe in 1972. The company was renamed Nike, Inc., in 1978 and went public two years later. By the early 21st century, Nike had retail outlets and distributors in more than 170 countries, and its logo—a curved check mark called the “swoosh”—was recognized throughout the world.', N'ValerianBlue.jpg', 4, N'nk', CAST(N'2021-02-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'nk2', N'Nike Run', 1223, N'Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight. They opened their first retail outlet in 1966 and launched the Nike brand shoe in 1972. The company was renamed Nike, Inc., in 1978 and went public two years later. By the early 21st century, Nike had retail outlets and distributors in more than 170 countries, and its logo—a curved check mark called the “swoosh”—was recognized throughout the world.', N'ArgonHyperRoyal.jpg', 5, N'nk', CAST(N'2021-02-15' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'nk3', N'Nike Low', 1150, N'Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight. They opened their first retail outlet in 1966 and launched the Nike brand shoe in 1972. The company was renamed Nike, Inc., in 1978 and went public two years later. By the early 21st century, Nike had retail outlets and distributors in more than 170 countries, and its logo—a curved check mark called the “swoosh”—was recognized throughout the world.', N'CACTUS Brown.jpg', 9, N'nk', CAST(N'2022-02-14' AS Date), N'Unisex')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'nk4', N'Nike hub', 1999, N'Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight. They opened their first retail outlet in 1966 and launched the Nike brand shoe in 1972. The company was renamed Nike, Inc., in 1978 and went public two years later. By the early 21st century, Nike had retail outlets and distributors in more than 170 countries, and its logo—a curved check mark called the “swoosh”—was recognized throughout the world.', N'LightMadderRoot.jpg', 8, N'nk', CAST(N'2022-02-14' AS Date), N'Unisex')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'nk5', N'Nike high', 2399, N'Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight. They opened their first retail outlet in 1966 and launched the Nike brand shoe in 1972. The company was renamed Nike, Inc., in 1978 and went public two years later. By the early 21st century, Nike had retail outlets and distributors in more than 170 countries, and its logo—a curved check mark called the “swoosh”—was recognized throughout the world.', N'PhilliesValorBlueandTeamMaroon.jpg', 7, N'nk', CAST(N'2022-02-14' AS Date), N'Unisex')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'nk6', N'Nike SB', 1299, N'Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight. They opened their first retail outlet in 1966 and launched the Nike brand shoe in 1972. The company was renamed Nike, Inc., in 1978 and went public two years later. By the early 21st century, Nike had retail outlets and distributors in more than 170 countries, and its logo—a curved check mark called the “swoosh”—was recognized throughout the world.', N'ReversePanda.jpg', 9, N'nk', CAST(N'2019-02-14' AS Date), N'Unisex')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'nk7', N'Nike Dunk', 1500, N'Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight. They opened their first retail outlet in 1966 and launched the Nike brand shoe in 1972. The company was renamed Nike, Inc., in 1978 and went public two years later. By the early 21st century, Nike had retail outlets and distributors in more than 170 countries, and its logo—a curved check mark called the “swoosh”—was recognized throughout the world.', N'St.Patrick’sDayShamrock.jpg', 1, N'nk', CAST(N'2019-02-14' AS Date), N'Unisex')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'nk8', N'Nike job', 1600, N'Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight. They opened their first retail outlet in 1966 and launched the Nike brand shoe in 1972. The company was renamed Nike, Inc., in 1978 and went public two years later. By the early 21st century, Nike had retail outlets and distributors in more than 170 countries, and its logo—a curved check mark called the “swoosh”—was recognized throughout the world.', N'WhitePatentLeather.jpg', 2, N'nk', CAST(N'2019-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'nk9', N'Nike tech', 1800, N'Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight. They opened their first retail outlet in 1966 and launched the Nike brand shoe in 1972. The company was renamed Nike, Inc., in 1978 and went public two years later. By the early 21st century, Nike had retail outlets and distributors in more than 170 countries, and its logo—a curved check mark called the “swoosh”—was recognized throughout the world.', N'WhiteYellow.jpg', 3, N'nk', CAST(N'2018-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'pm1', N'Puma 1', 200, N'PUMA has relentlessly pushed sport and culture forward by creating fast products for the world’s fastest athletes. For more than 70 years, we draw strength and credibility from our heritage in sports. ', N'Screenshot 2022-06-18 154214.png', 2, N'pm', CAST(N'2019-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'pm10', N'Puma 10', 260, N'PUMA has relentlessly pushed sport and culture forward by creating fast products for the world’s fastest athletes. For more than 70 years, we draw strength and credibility from our heritage in sports. ', N'Screenshot 2022-06-18 154410.png', 6, N'pm', CAST(N'2019-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'pm2', N'Puma 2', 300, N'PUMA has relentlessly pushed sport and culture forward by creating fast products for the world’s fastest athletes. For more than 70 years, we draw strength and credibility from our heritage in sports. ', N'Screenshot 2022-06-18 154230.png', 4, N'pm', CAST(N'2019-02-14' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'pm3', N'Puma 3', 420, N'PUMA has relentlessly pushed sport and culture forward by creating fast products for the world’s fastest athletes. For more than 70 years, we draw strength and credibility from our heritage in sports. ', N'Screenshot 2022-06-18 154241.png', 5, N'pm', CAST(N'2019-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'pm4', N'Puma 4', 560, N'PUMA has relentlessly pushed sport and culture forward by creating fast products for the world’s fastest athletes. For more than 70 years, we draw strength and credibility from our heritage in sports. ', N'Screenshot 2022-06-18 154250.png', 6, N'pm', CAST(N'2019-02-14' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'pm5', N'Puma 5', 120, N'PUMA has relentlessly pushed sport and culture forward by creating fast products for the world’s fastest athletes. For more than 70 years, we draw strength and credibility from our heritage in sports. ', N'Screenshot 2022-06-18 154300.png', 7, N'pm', CAST(N'2019-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'pm6', N'Puma 6', 36, N'PUMA has relentlessly pushed sport and culture forward by creating fast products for the world’s fastest athletes. For more than 70 years, we draw strength and credibility from our heritage in sports. ', N'Screenshot 2022-06-18 154312.png', 8, N'pm', CAST(N'2019-02-14' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'pm7', N'Puma 7', 360, N'PUMA has relentlessly pushed sport and culture forward by creating fast products for the world’s fastest athletes. For more than 70 years, we draw strength and credibility from our heritage in sports. ', N'Screenshot 2022-06-18 154321.png', 9, N'pm', CAST(N'2019-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'pm8', N'Puma 8', 120, N'PUMA has relentlessly pushed sport and culture forward by creating fast products for the world’s fastest athletes. For more than 70 years, we draw strength and credibility from our heritage in sports. ', N'Screenshot 2022-06-18 154338.png', 4, N'pm', CAST(N'2019-02-14' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'pm9', N'Puma 9', 253, N'PUMA has relentlessly pushed sport and culture forward by creating fast products for the world’s fastest athletes. For more than 70 years, we draw strength and credibility from our heritage in sports. ', N'Screenshot 2022-06-18 154400.png', 8, N'pm', CAST(N'2019-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'vs1', N'Vans ols', 420, N'After working for some years at Randy''s, a shoe manufacturer, Paul Van Doren decided he wanted to start his own shoe brand. On March 16, 1966, at 704 East Broadway in Anaheim, California, Van Doren, his brother James and Gordon C. Lee opened the first Vans store under the name The Van Doren Rubber Company.', N'Screenshot 2022-06-18 154455.png', 2, N'vs', CAST(N'2019-02-14' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'vs10', N'Vans 9', 120, N'After working for some years at Randy''s, a shoe manufacturer, Paul Van Doren decided he wanted to start his own shoe brand. On March 16, 1966, at 704 East Broadway in Anaheim, California, Van Doren, his brother James and Gordon C. Lee opened the first Vans store under the name The Van Doren Rubber Company.', N'Screenshot 2022-06-18 154732.png', 9, N'vs', CAST(N'2019-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'vs2', N'Vans 1', 120, N'After working for some years at Randy''s, a shoe manufacturer, Paul Van Doren decided he wanted to start his own shoe brand. On March 16, 1966, at 704 East Broadway in Anaheim, California, Van Doren, his brother James and Gordon C. Lee opened the first Vans store under the name The Van Doren Rubber Company.', N'Screenshot 2022-06-18 154511.png', 5, N'vs', CAST(N'2019-02-14' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'vs3', N'Vans 2', 300, N'After working for some years at Randy''s, a shoe manufacturer, Paul Van Doren decided he wanted to start his own shoe brand. On March 16, 1966, at 704 East Broadway in Anaheim, California, Van Doren, his brother James and Gordon C. Lee opened the first Vans store under the name The Van Doren Rubber Company.', N'Screenshot 2022-06-18 154522.png', 6, N'vs', CAST(N'2018-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'vs4', N'Vans 3', 150, N'After working for some years at Randy''s, a shoe manufacturer, Paul Van Doren decided he wanted to start his own shoe brand. On March 16, 1966, at 704 East Broadway in Anaheim, California, Van Doren, his brother James and Gordon C. Lee opened the first Vans store under the name The Van Doren Rubber Company.', N'Screenshot 2022-06-18 154534.png', 9, N'vs', CAST(N'2019-02-14' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'vs5', N'Vans 4', 140, N'After working for some years at Randy''s, a shoe manufacturer, Paul Van Doren decided he wanted to start his own shoe brand. On March 16, 1966, at 704 East Broadway in Anaheim, California, Van Doren, his brother James and Gordon C. Lee opened the first Vans store under the name The Van Doren Rubber Company.', N'Screenshot 2022-06-18 154623.png', 8, N'vs', CAST(N'2018-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'vs6', N'Vans 5', 160, N'After working for some years at Randy''s, a shoe manufacturer, Paul Van Doren decided he wanted to start his own shoe brand. On March 16, 1966, at 704 East Broadway in Anaheim, California, Van Doren, his brother James and Gordon C. Lee opened the first Vans store under the name The Van Doren Rubber Company.', N'Screenshot 2022-06-18 154623.png', 6, N'vs', CAST(N'2019-02-14' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'vs7', N'Vans 6', 120, N'After working for some years at Randy''s, a shoe manufacturer, Paul Van Doren decided he wanted to start his own shoe brand. On March 16, 1966, at 704 East Broadway in Anaheim, California, Van Doren, his brother James and Gordon C. Lee opened the first Vans store under the name The Van Doren Rubber Company.', N'Screenshot 2022-06-18 154644.png', 7, N'vs', CAST(N'2019-02-14' AS Date), N'Male')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'vs8', N'Vans 7', 90, N'After working for some years at Randy''s, a shoe manufacturer, Paul Van Doren decided he wanted to start his own shoe brand. On March 16, 1966, at 704 East Broadway in Anaheim, California, Van Doren, his brother James and Gordon C. Lee opened the first Vans store under the name The Van Doren Rubber Company.', N'Screenshot 2022-06-18 154656.png', 5, N'vs', CAST(N'2019-02-14' AS Date), N'Female')
INSERT [dbo].[Product] ([ID], [Name], [Price], [Describe], [image], [quantity], [Cid], [releaseDate], [sex]) VALUES (N'vs9', N'Vans 8', 199, N'After working for some years at Randy''s, a shoe manufacturer, Paul Van Doren decided he wanted to start his own shoe brand. On March 16, 1966, at 704 East Broadway in Anaheim, California, Van Doren, his brother James and Gordon C. Lee opened the first Vans store under the name The Van Doren Rubber Company.', N'Screenshot 2022-06-18 154710.png', 8, N'vs', CAST(N'2019-02-14' AS Date), N'Female')
GO
INSERT [dbo].[ProductSale] ([pid], [priceSale]) VALUES (N'ad3', 2000)
INSERT [dbo].[ProductSale] ([pid], [priceSale]) VALUES (N'ad6', 1000)
INSERT [dbo].[ProductSale] ([pid], [priceSale]) VALUES (N'cv6', 300)
INSERT [dbo].[ProductSale] ([pid], [priceSale]) VALUES (N'jd4', 1000)
INSERT [dbo].[ProductSale] ([pid], [priceSale]) VALUES (N'jd9', 1500)
INSERT [dbo].[ProductSale] ([pid], [priceSale]) VALUES (N'nk3', 1000)
GO
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'1', N'ad1', N'9 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'2', N'ad2', N'9.5 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'3', N'ad3', N'10 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'4', N'jd1', N'9 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'5', N'jd2', N'9.5 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'6', N'jd3', N'9 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'7', N'cv1', N'8 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'8', N'cv2', N'9 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'9', N'cv3', N'9.5 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'10', N'jd1', N'9.5 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'11', N'jd2', N'8.5 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'12', N'jd3', N'10 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'13', N'nk1', N'10 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'14', N'nk2', N'8 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'15', N'nk3', N'10 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'16', N'pm1', N'8 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'17', N'pm2', N'8.5 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'18', N'pm3', N'10 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'19', N'vs1', N'8 UK')
INSERT [dbo].[Size] ([ID], [pid], [Size]) VALUES (N'20', N'vs1', N'8.5 UK')
GO
INSERT [dbo].[Users] ([Username], [Password], [Phone], [Email], [address], [Fullname], [roll]) VALUES (N'sa', N'123', N'19001586', N'abc@gmail.com', N'Ha noi', N'Nguyen van a', N'Admin')
INSERT [dbo].[Users] ([Username], [Password], [Phone], [Email], [address], [Fullname], [roll]) VALUES (N'sb', N'123', N'15382456', N'xyz@gmail.com', N'Ha noi', N'Nguyen van b', N'User')
INSERT [dbo].[Users] ([Username], [Password], [Phone], [Email], [address], [Fullname], [roll]) VALUES (N'sc', N'123', N'12851215', N'aic@gmail.com', N'Ha noi', N'Nguyen van c', N'User')
INSERT [dbo].[Users] ([Username], [Password], [Phone], [Email], [address], [Fullname], [roll]) VALUES (N'sd', N'abc', N'a', N'a', N'a', N'a', N'User')
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([Username])
REFERENCES [dbo].[Users] ([Username])
GO
ALTER TABLE [dbo].[Ordersdetails]  WITH CHECK ADD FOREIGN KEY([ordID])
REFERENCES [dbo].[Orders] ([ID])
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([Cid])
REFERENCES [dbo].[Category] ([ID])
GO
ALTER TABLE [dbo].[ProductSale]  WITH CHECK ADD FOREIGN KEY([pid])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[Size]  WITH CHECK ADD FOREIGN KEY([pid])
REFERENCES [dbo].[Product] ([ID])
GO
USE [master]
GO
ALTER DATABASE [SNSSneaker] SET  READ_WRITE 
GO
